package com.lumos.lusistant.backendserver.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "audio")

public class Audio {


    private String recoded_audio;
    private String person;

    public String getRecoded_audio() {

        return recoded_audio;
    }

    public void setRecoded_audio(String recoded_audio) {

        this.recoded_audio = recoded_audio;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }
}
