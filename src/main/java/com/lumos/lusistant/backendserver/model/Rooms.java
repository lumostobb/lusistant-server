package com.lumos.lusistant.backendserver.model;

import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "rooms")

public class Rooms implements Serializable {
    private String roomName;
    private int numberOfLights;
    private List<User> userList = new ArrayList<>();


    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {

        this.roomName = roomName;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public int getNumberOfLights() {

        return numberOfLights;
    }

    public void setNumberOfLights(int numberOfLights)
    {
        this.numberOfLights = numberOfLights;
    }

    public void addUserToList(User user){
        userList.add(user);
    }

}
