package com.lumos.lusistant.backendserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.io.*;
import java.net.*;
import java.util.Enumeration;
import java.util.HashMap;

@SpringBootApplication
public class BackendServerApplication {

    public static String ipResult = "";
    private static final String POST_URL = "http://lumos-ip.herokuapp.com/ipBackend";

    public static void main(String[] args) throws IOException {
        SpringApplication.run(BackendServerApplication.class, args);

        String line = "";

        String url = "http://lumos-ip.herokuapp.com/ip";
        URL taskRequestUrl = new URL(url);
        HttpURLConnection httpURLConnection = (HttpURLConnection) taskRequestUrl.openConnection();
        InputStream inputStream = httpURLConnection.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder builder = new StringBuilder();

        line = "";
        String[] ip= new String[15];
        while ((line = bufferedReader.readLine()) != null) {

            ip = line.split(":");
        }
        ipResult = ip[0];

        String ip2 = "";
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                // filters out 127.0.0.1 and inactive interfaces
                if (iface.isLoopback() || !iface.isUp())
                    continue;

                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while(addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();

                    // *EDIT*
                    if (addr instanceof Inet6Address) continue;

                    ip2 = addr.getHostAddress();
                }
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }

        System.out.println("IP: " + ip2);
        sendPOST(ip2);
    }

    private static void sendPOST(String ip) throws IOException {
        URL url = new URL(POST_URL + "/" + ip);

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        int responseCode = conn.getResponseCode();
        System.out.println("Ip gonderildi");
    }


}

