package com.lumos.lusistant.backendserver.repository;

import com.lumos.lusistant.backendserver.model.Rooms;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoomsRepository extends MongoRepository<Rooms, String> {
    Rooms findByRoomName(String roomName);
}
