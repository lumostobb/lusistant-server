package com.lumos.lusistant.backendserver.repository;

import com.lumos.lusistant.backendserver.model.Audio;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface AudioRepository extends MongoRepository<Audio, String> {
}