package com.lumos.lusistant.backendserver.repository;

import com.lumos.lusistant.backendserver.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {
    User findByUserName(String userName);
    long deleteByUserName(String userName);

}
