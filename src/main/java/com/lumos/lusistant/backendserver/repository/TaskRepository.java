package com.lumos.lusistant.backendserver.repository;

import com.lumos.lusistant.backendserver.model.Task;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TaskRepository extends MongoRepository<Task, String> {
    Task findByNameOfTask(String nameOfTask);
}