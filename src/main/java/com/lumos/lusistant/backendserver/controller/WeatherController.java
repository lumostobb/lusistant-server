package com.lumos.lusistant.backendserver.controller;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@RestController

public class WeatherController {

    @GetMapping("/runWeather")
    public ResponseEntity<String> getWeather() throws IOException {
        //String url = "http://api.openweathermap.org/data/2.5/forecast?q=Ankara,tr&APPID=3a5d9f9d7985cd6d8629fc16bb3e1d2d";
        String url = "http://api.openweathermap.org/data/2.5/weather?q=Ankara,Turkey&APPID=3a5d9f9d7985cd6d8629fc16bb3e1d2d";

        double kelvin = 0;
        double celsius = 0;
        Object situation="";
        String result = "";
        try {
            URL taskRequestUrl = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) taskRequestUrl.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder builder = new StringBuilder();

            String line = "";
            while (line != null) {
                line = bufferedReader.readLine();
                builder.append(line);
            }

            JSONObject object = new JSONObject(builder.toString());
            JSONObject rates_object = new JSONObject(object.getJSONObject("main").toString());
            System.out.println(object);
            kelvin = rates_object.getDouble("temp");
            celsius = kelvin - 273.0;
            System.out.println ("\n" + kelvin + "K = "+ celsius + "C");
            //System.out.println("Case: " +  rates_object.getDouble("temp"));


            //System.out.println((object.get("weather")));
            Object aa = object.get("weather");

            //System.out.println((object.getJSONArray("weather")).get(0));
            JSONObject arr = new JSONObject();
            arr = (JSONObject) object.getJSONArray("weather").get(0);
            situation = arr.get("main");
            //System.out.println(arr.get("main"));
            result = getInfo(situation,(int)celsius);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new  ResponseEntity(result, HttpStatus.OK);
    }

    public String getInfo(Object situation,int celcius){
        String result;
        if(situation.equals("Rain")) {
            result = "Şemsiye almalısın. "+ " ,"+ "Hava:"+ (celcius) + " derece";
            System.out.print("Şemsiye almalısın. "+ " ,"+ "Hava:"+ (celcius) + " derece");
        }
        if(situation.equals("Clouds")) {
            result = "Hava Bulutlu"+ ". "+ "Hava: "+ (celcius) + " derece";
            System.out.print("Hava Bulutlu"+ ". "+ "Hava: "+ (celcius) + " derece");
        }
        else{
            result = "Hava " + celcius + "derece";
        }
        return result;
    }

}