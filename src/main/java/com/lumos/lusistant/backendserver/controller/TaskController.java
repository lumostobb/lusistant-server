package com.lumos.lusistant.backendserver.controller;

import com.lumos.lusistant.backendserver.BackendServerApplication;
import com.lumos.lusistant.backendserver.model.Task;
import com.lumos.lusistant.backendserver.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

@RestController
public class TaskController {

    @Autowired
    TaskRepository taskRepository;

    @PostMapping("/CreateTask")
    public ResponseEntity createTask(@RequestBody Task task){
        Task tempTasks = taskRepository.save(task);
        if(tempTasks == null) {
            return new ResponseEntity<>("Hata! Görev Oluşturulamadı!", HttpStatus.NOT_FOUND);

        }
        return new ResponseEntity<>("Görev Başarıyla Oluşturuldu!", HttpStatus.OK);
    }

    @GetMapping("/getAllTasks")
    public ResponseEntity<List> getAllTask(){
        List<Task> taskList = taskRepository.findAll();
        if(taskList == null){
            return new ResponseEntity(null,HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(taskList,HttpStatus.OK);
    }

    @GetMapping("/getAllUrl")
    public ResponseEntity<List> getAllUrl(){
        List<Task> urlList = taskRepository.findAll();
        List<String> urlNames = new ArrayList<>();
        for(Task temp_url: urlList){
            urlNames.add(temp_url.getUrl());
        }
        if(urlNames.size() == 0){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(urlNames, HttpStatus.OK);
    }

    @GetMapping("/deneme")
    public String SesGetirPyhton() throws IOException {

        String command = "python2 /home/toorn/Desktop/enson_speaker/Speaker-identification/test_speaker.py /home/toorn/Downloads/can1.wav";
        Process p = Runtime.getRuntime().exec(command);
        BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String ret = in.readLine();
        System.out.println("value is : "+ret);
        return ret;
    }

    @GetMapping("/runTask/{command}")
    public ResponseEntity<String> getTask(@PathVariable String command) throws IOException {
        File f = new File("intentClassifier/Asil.py");
        System.out.println(f.getAbsoluteFile());
        String u = URLDecoder.decode(command,"utf-8");
        System.out.println(u);
        String command2 = "python3";
        String command3 = "/home/toorn/Desktop/intentClassifier/Asil.py";

        Process p = Runtime.getRuntime().exec(new String[]{command2, command3, u});
        BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
        StringBuilder builder = new StringBuilder();
        String line = "";
        String line2 = "";
        while(1==1){
            line2 = line;
            line = in.readLine();
            if(line == null){
                break;
            }
        }
        System.out.println(line2);
        Task task = taskRepository.findByNameOfTask(line2);
        String url = task.getUrl();
        String ipAddress = BackendServerApplication.ipResult;

        String urlResult = "http://" + ipAddress + ":8081/" + url;
        String line3 = "";
        String line4 = "";
        try {
            URL taskRequestUrl = new URL(urlResult);
            HttpURLConnection httpURLConnection = (HttpURLConnection) taskRequestUrl.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            StringBuilder builder2 = new StringBuilder();

            while(1==1){
                line4 = line3;
                line3 = bufferedReader.readLine();
                if(line3 == null){
                    break;
                }
            }

            System.out.println(line4);

        } catch (IOException e) {
            e.printStackTrace();
        }
        if(task == null){
            return new ResponseEntity(line4, HttpStatus.NOT_FOUND);
        }

        return new  ResponseEntity(line4,HttpStatus.OK);
    }

    @GetMapping("/getIpAdres")
    public String getIpAdres() throws IOException {
        String url = "http://lumos-ip.herokuapp.com/ip";
        URL taskRequestUrl = new URL(url);
        HttpURLConnection httpURLConnection = (HttpURLConnection) taskRequestUrl.openConnection();
        InputStream inputStream = httpURLConnection.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder builder = new StringBuilder();

        String line = "";
        String[] ip= new String[15];


        while ((line = bufferedReader.readLine()) != null) {

            ip = line.split(":");
        }
        return ip[0];
    }

    @GetMapping("/speakerRecognition")
    public String speakerRecognition() throws IOException {
        String filePath = "/home/toorn/Desktop/BitirmeProjesi/lusistant-server/res/recognito.jar";
        String command5 = "java";
        String command6 = "-jar";
        String command7 = filePath;

        Process p2 = Runtime.getRuntime().exec(new String[]{command5, command6, command7});

        String command2 = "python2";
        String command3 = "/home/toorn/Desktop/Speaker-identification/test_speaker.py";
        String command4 = "/home/toorn/Public/output.wav";

        Process p = Runtime.getRuntime().exec(new String[]{command2, command3, command4});
        BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line = "";
        String line2 = "";
        while(1==1){
            line2 = line;
            line = in.readLine();
            if(line == null){
                break;
            }
        }
        System.out.println(line2);

        return line2;
    }


}
