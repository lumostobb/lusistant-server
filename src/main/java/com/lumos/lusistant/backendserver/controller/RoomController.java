package com.lumos.lusistant.backendserver.controller;

import com.lumos.lusistant.backendserver.model.Rooms;
import com.lumos.lusistant.backendserver.model.User;
import com.lumos.lusistant.backendserver.repository.RoomsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RoomController {
    @Autowired
    RoomsRepository RoomsRepository;

    @PostMapping("/CreateRooms")
    public ResponseEntity<String> createRoom(@RequestBody Rooms rooms){
        Rooms tempRooms = RoomsRepository.save(rooms);
        if(tempRooms == null){
            return new ResponseEntity<>("Hata! Odalar Oluşturulamadı!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>("Odalar Başarıyla Oluşturuldu!", HttpStatus.OK);
    }

    // Butun odaları al
    @GetMapping("/getAllRooms")
    public ResponseEntity<List> getAllUser(){
        //find all metodu ile butun odaları alıyoruz.
        List<Rooms> roomList = RoomsRepository.findAll();
        System.out.println(roomList);
        if(roomList== null){
            System.out.println("girdi");
            return new ResponseEntity(null,HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(roomList,HttpStatus.OK);
    }


}
