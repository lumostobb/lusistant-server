package com.lumos.lusistant.backendserver.controller;

import com.lumos.lusistant.backendserver.model.Rooms;
import com.lumos.lusistant.backendserver.model.User;
import com.lumos.lusistant.backendserver.repository.RoomsRepository;
import com.lumos.lusistant.backendserver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoomsRepository roomsRepository;

    @PostMapping("/userRegister")
    ResponseEntity<String> userRegister(@RequestBody User user) {
        User tempUser = userRepository.save(user);
        if(tempUser == null){
            return new ResponseEntity<>("Hata! Kayıt Oluşturulamadı!", HttpStatus.NOT_FOUND);

        }
        return new ResponseEntity<>("Kayıt Başarıyla Oluşturuldu!", HttpStatus.OK);
    }

    @GetMapping("/getAllUsers")
     public ResponseEntity<List> getAllUser(){
        List<User> userList = userRepository.findAll();
        if(userList== null){
            return new ResponseEntity(null,HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(userList,HttpStatus.OK);
    }

    @GetMapping("/getAllUsersName")
    public ResponseEntity<List> getAllUsers_Name(){
        List<User> userList = userRepository.findAll();
        List<String> userNames = new ArrayList<>();
        if(userList != null) {
            for (User temp_user : userList) {
                userNames.add(temp_user.getUserName());
            }
        }else{
            return new ResponseEntity(null,HttpStatus.NOT_FOUND);

        }
        return new ResponseEntity(userNames,HttpStatus.OK);
    }

    @GetMapping("/{user_name}")
    public ResponseEntity getUserInfo(@PathVariable String user_name){
        User resultUser = userRepository.findByUserName(user_name);
        if(resultUser == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new  ResponseEntity(resultUser, HttpStatus.OK);
    }

    @DeleteMapping("/{user_name}")
    public ResponseEntity<String> getDeleteUser(@PathVariable String user_name){
        long resultUser = userRepository.deleteByUserName(user_name);
        if(resultUser == 0){
            return new ResponseEntity(null,HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity("Kullanıcı Silindi.",HttpStatus.OK);
    }



    @RequestMapping(value = "/userRegister/{user_name}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@PathVariable("user_name") String user_name, @RequestBody User user) {

        User currentUser = userRepository.findByUserName(user_name);

        if (currentUser == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if (user.getSurname() != null) {
            currentUser.setSurname(user.getSurname());
        }
        if (user.getName() != null) {
            currentUser.setName(user.getName());
        }

        userRepository.save(currentUser);
        return new ResponseEntity<User>(currentUser, HttpStatus.OK);
    }

}
